
# Seinfeld script generation

The goal of this project is to generate our own [Seinfeld](https://en.wikipedia.org/wiki/Seinfeld) TV scripts using RNNs.  We'll be using part of the [Seinfeld dataset](https://www.kaggle.com/thec03u5/seinfeld-chronicles#scripts.csv) of scripts from 9 seasons. The Neural Network we'll build will generate a new ,"fake" TV script, based on patterns it recognizes in this training data.

# Overall steps

The project consists of the following steps:

1. Get the data
2. Explore the data
3. Preprocess the data
4. Build the RNN-based network
5. Select hyperparameters
6. Train the network
7. Generate new TV script

## Repository

The original Udacity project instructions can be found [here](https://github.com/udacity/deep-learning-v2-pytorch/tree/master/project-tv-script-generation).


## Dependencies

The information regarding dependencies for the project can be obtained from [here](https://github.com/udacity/deep-learning-v2-pytorch).

## Detailed Writeup

Detailed reports can be found in [Word level LSTM pipeline](./Seinfeld_script_generation_word_level_LSTM.ipynb) and 
[Character level LSTM pipeline](./Seinfeld_script_generation_char_level_LSTM.ipynb).

## Acknowledgments

I would like to thank Udacity for giving me this opportunity to work on an awesome project. 

